[![pipeline status]](https://gitlab.com/zoomonit/greenpepper/pipelines)
[![Maven Central]](http://mvnrepository.com/artifact/com.github.strator-dev.greenpepper)
[![Bintray]](https://bintray.com/greenpepper/generic/greenpepper/_latestVersion)
[![Codacy Badge]](https://www.codacy.com/app/ZoomOnIT/greenpepper?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=zoomonit/greenpepper&amp;utm_campaign=Badge_Grade)

[![Slack.svg]](https://join.slack.com/t/greenpepper-app/shared_invite/enQtMjg3MjQ1MzIzNzQ0LTI2M2E3ZTgxZTM1ZjdlNzU1YmQ2ODU5MTQ0M2VkMTUyMTY4OWExMDI4MTM5NmRkNWZiZjI5ZTU5OTdlMDQyNDE)

[pipeline status]: https://gitlab.com/zoomonit/greenpepper/badges/master/pipeline.svg
[Maven Central]: https://img.shields.io/maven-central/v/com.github.strator-dev.greenpepper/greenpepper-maven-plugin.svg
[Bintray]: https://img.shields.io/bintray/v/greenpepper/generic/greenpepper.svg
[Codacy Badge]: https://api.codacy.com/project/badge/Grade/74fac89bca414061ab00c1379a0d650f
[Gitlab]: https://gitlab.com/zoomonit/greenpepper/

# [GreenPepper](https://greenpepper.atlassian.net/wiki)


| Support | Issues | Contributions |
|:---------:|:--------:|:--------------:|
| [![support.png]](https://join.slack.com/t/greenpepper-app/shared_invite/enQtMjg3MjQ1MzIzNzQ0LTI2M2E3ZTgxZTM1ZjdlNzU1YmQ2ODU5MTQ0M2VkMTUyMTY4OWExMDI4MTM5NmRkNWZiZjI5ZTU5OTdlMDQyNDE) | [![questions.png]](https://gitlab.com/zoomonit/greenpepper/issues) | [![contribute.png]](https://greenpepper.atlassian.net/wiki/display/DOC/Contribute+to+Code) |
| In case you still have questions regarding the plugin's usage, please have a look at the [Documentation](http://greenpepper.atlassian.net) and feel free to contact us on [![Slack.svg]](https://join.slack.com/t/greenpepper-app/shared_invite/enQtMjg3MjQ1MzIzNzQ0LTI2M2E3ZTgxZTM1ZjdlNzU1YmQ2ODU5MTQ0M2VkMTUyMTY4OWExMDI4MTM5NmRkNWZiZjI5ZTU5OTdlMDQyNDE). | If you feel like the plugin is missing a feature or has a defect, you can fill a feature request or bug report in our [issue tracker]. Questions can also be asked there.When creating a new issue, please provide a comprehensive description of your concern. Especially for fixing bugs it is crucial that the developers can reproduce your problem. For this reason, entire debug logs, POMs or most preferably little demo projects attached to the issue are very much appreciated. | Of course, patches are welcome, too. Contributors can clone the project from our [source repository] and will find supplementary information in the [guide to Developing GreenPepper]. |


## Current version : [![Maven Central]](http://mvnrepository.com/artifact/com.github.strator-dev.greenpepper)

**Implemented enhancements:**

- Maven plugin reports type is not relevant anymore [\#118](https://github.com/strator-dev/greenpepper/issues/118)

**Fixed bugs:**

- GreenPepper client conflicts due to org/xml/sax [\#121](https://github.com/strator-dev/greenpepper/issues/121)

**Closed issues:**

- Update mvn plugin documentation [\#53](https://github.com/strator-dev/greenpepper/issues/53)

**Merged pull requests:**

- Drop greenpepper license project [\#120](https://github.com/strator-dev/greenpepper/pull/120) ([wattazoum](https://github.com/wattazoum))

Check [The Full Changelog for more information](CHANGELOG.md)


# Downloads

The Downloads are availaible on [our WIKI](https://greenpepper.atlassian.net/wiki)

## Using Maven, Gradle and the like

[![Maven Central](https://img.shields.io/maven-central/v/com.github.strator-dev.greenpepper/greenpepper-maven-plugin.svg)](http://mvnrepository.com/artifact/com.github.strator-dev.greenpepper)

The artifacts are available on [Central](http://mvnrepository.com/artifact/com.github.strator-dev.greenpepper)

# Acknowledgments

This project is given to you using help and resources from:


| Company |  |
| ----------- | ------------ |
| [![Yourkit Logo]](https://www.yourkit.com) |	YourKit supports open source projects with its full-featured Java Profiler. YourKit, LLC is the creator of [YourKit Java Profiler] and [YourKit .NET Profiler], innovative and intelligent tools for profiling Java and .NET applications.|
| [![IDEA Logo]](https://www.jetbrains.com/idea/) | JetBrains for granting us an open-source license of their IntelliJ IDEA environment. |
| [![Confluence Logo]](https://www.atlassian.com/software/confluence) | Atlassian for providing us with a High quality WIKI.|



[Yourkit]: https://www.yourkit.com
[YourKit Java Profiler]: https://www.yourkit.com/java/profiler/index.jsp
[YourKit .NET Profiler]: https://www.yourkit.com/.net/profiler/index.jsp
[Slack]: https://join.slack.com/t/greenpepper-app/shared_invite/enQtMjg3MjQ1MzIzNzQ0LTI2M2E3ZTgxZTM1ZjdlNzU1YmQ2ODU5MTQ0M2VkMTUyMTY4OWExMDI4MTM5NmRkNWZiZjI5ZTU5OTdlMDQyNDE
[issue tracker]: https://gitlab.com/zoomonit/greenpepper/issues
[source repository]: https://gitlab.com/zoomonit/greenpepper
[guide to Developing GreenPepper]: https://greenpepper.atlassian.net/wiki/display/DOC/Contribute+to+Code

[support.png]: http://strator-dev.github.io/greenpepper/images/support.png
[questions.png]: http://strator-dev.github.io/greenpepper/images/questions.png
[contribute.png]: http://strator-dev.github.io/greenpepper/images/contribute.png
[IDEA Logo]: http://strator-dev.github.io/greenpepper/images/icon_IntelliJIDEA.png
[Confluence Logo]: http://strator-dev.github.io/greenpepper/images/confluence_rgb_blue.png
[Yourkit Logo]: https://www.yourkit.com/images/yklogo.png
[gitlab.png]: src/site/resources/images/gitlab.png
[Slack.svg]: src/site/resources/images/slack.png