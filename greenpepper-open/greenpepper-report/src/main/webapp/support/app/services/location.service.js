(function() {
    "use strict";

    angular.module("greenpepper.services").factory("LocationService", LocationService);

    LocationService.$inject = ["$location", "$log"];

    function LocationService($location, $log) {

        function addCount(count) {
            __handleQueryParam("count", count);
            $log.debug("addCount =>" + $location.url());
        }

        function addResultState(resultState) {
            __handleQueryParam("state", resultState);
          $log.debug("addResultState =>" + $location.url());
        }

        function addTrust(trust) {
          __handleQueryParam("trusted", trust);
          $log.debug("addTrust =>" + $location.url());
        }

        function addSearch(searchString) {
          __handleQueryParam("search", searchString);
          $log.debug("addSearch =>" + $location.url());
        }

        function addRepo(repoName) {
            __handleQueryParam("repo", repoName);
            $log.debug("addRepo =>" + $location.url());
        }

        function getParams() {
            return angular.copy($location.search());
        }

        function __handleQueryParam(paramName, value) {
            if (angular.isUndefined(value)){
                $location.search(paramName, null);
            } else {
                $location.search(paramName, String(value));
            }
        }

        return {
            addCount: addCount,
            addResultState: addResultState,
            addTrust: addTrust,
            addSearch: addSearch,
            addRepo: addRepo,
            getParams: getParams
        }
    }

})();