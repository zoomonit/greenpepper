(function () {
    "use strict";

    angular.module('greenpepper.report', ['greenpepper.services', 'ngTable', 'ui.bootstrap', 'ngSanitize', 'chart.js']);

})();