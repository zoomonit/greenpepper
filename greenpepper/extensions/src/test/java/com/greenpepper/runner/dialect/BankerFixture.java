package com.greenpepper.runner.dialect;

public class BankerFixture {

    public boolean openAnAccountFor(String name) {
        return true;
    }

    public String theIdOfAccount(String name) {
        return "abc";
    }

    public long theBalanceOfAccountIs(String name) {
        return 0L;
    }
}
