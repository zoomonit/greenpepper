package com.greenpepper.runner.dialect;

import com.greenpepper.dialect.SpecificationDialect;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

abstract class AbstractOpenDocumentDialect implements SpecificationDialect {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractOpenDocumentDialect.class);

    String cleanUpTheHtml(String source) {
        Document jsoup = Jsoup.parse(source);
        removeParagraphesInLi(jsoup);
        unwrapTheTitlesFromLi(jsoup);

        String newResult = jsoup.toString();
        LOGGER.debug("result of conversion: \n{}", newResult);
        return newResult;
    }

    private void removeParagraphesInLi(final Document jsoup) {
        // the conversion puts breakings <p> inside the <li>s
        jsoup.body().select("li>p").unwrap();
    }

    private void unwrapTheTitlesFromLi(final Document jsoup){
        jsoup.select("ul>li>h1, ul>li>h2, ul>li>h3, ul>li>h4, ul>li>h5, ul>li>h6")
                .parents().unwrap();
    }
}
