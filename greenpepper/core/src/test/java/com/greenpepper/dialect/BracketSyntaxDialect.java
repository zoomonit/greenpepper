package com.greenpepper.dialect;


import static java.lang.String.format;
import static org.apache.commons.io.IOUtils.readLines;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

import java.io.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BracketSyntaxDialect implements SpecificationDialect {

    @Override
    public String convert(InputStream input) throws SpecificationDialectException {
        StringWriter writer = new StringWriter();
        try {
            writer.write("<html><body>\n");

            boolean isInTable = false;
            List<String> lines = readLines(input);
            for (String line : lines) {
                if (line.matches("^\\[(rule for|list of|set of|setup)](\\[[^]]+])+")) {
                    isInTable = true;
                    writer.write("<table>\n");
                    parseLine(writer, line);
                } else if (isNotBlank(line)) {
                    if (isInTable) {
                        parseLine(writer, line);
                    } else {
                        writer.write(format("<p>%s</p>\n", line));
                    }
                } else if (isBlank(line)) {
                    if (isInTable) {
                        writer.write("</table>\n");
                        isInTable = false;
                    } else {
                        writer.write("\n");
                    }
                }
            }
            if (isInTable) {
                writer.write("</table>\n");
            }

            writer.write("</body></html>\n");
            writer.flush();
            return writer.toString();
        } catch (IOException e) {
            throw new SpecificationDialectException("Expection parsing the input", e);
        }
    }

    private void parseLine(Writer writer, String line) throws IOException {
        String block = "\\[([^]]+)]";
        Pattern pattern = Pattern.compile("("+block+")+");
        Matcher matcher = pattern.matcher(line);
        if (!matcher.matches()) {
            throw new SpecificationDialectException(format("The line %s doesn't match the dialect", line));
        }

        writer.write("\t<tr>\n");
        Matcher blockMatcher = Pattern.compile(block).matcher(line);
        while (blockMatcher.find()) {
            writer.write(format("\t\t<td>%s</td>\n", blockMatcher.group(1)));
        }
        writer.write("\t</tr>\n");
    }
}