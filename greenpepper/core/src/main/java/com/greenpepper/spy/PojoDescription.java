package com.greenpepper.spy;

import java.util.Collection;

public interface PojoDescription extends SpyDescription {

    /**
     * @return the list of properties of this POJO
     */
    Collection<PropertyDescription> getProperties();
}
