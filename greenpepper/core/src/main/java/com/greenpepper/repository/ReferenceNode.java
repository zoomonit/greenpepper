package com.greenpepper.repository;

import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Vector;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * <p>ReferenceNode class.</p>
 *
 * @author oaouattara
 * @version $Id: $Id
 */
public class ReferenceNode extends DocumentNode implements Marshalizable
{

	/** Constant <code>NODE_REPOSITORY_UID_INDEX=4</code> */
	private final static int NODE_REPOSITORY_UID_INDEX = 4;
	/** Constant <code>NODE_SUT_NAME_INDEX=5</code> */
	private final static int NODE_SUT_NAME_INDEX = 5;
	/** Constant <code>NODE_SECTION_INDEX=6</code> */
	private final static int NODE_SECTION_INDEX = 6;
	private static final int NODE_URL_INDEX = 7;

	private String repositoryUID;
	private String sutName;
	private String section;

	/**
	 * <p>Constructor for ReferenceNode.</p>
	 *
	 * @param title a {@link java.lang.String} object.
	 * @param repositoryUID a {@link java.lang.String} object.
	 * @param sutName a {@link java.lang.String} object.
	 * @param section a {@link java.lang.String} object.
	 */
	public ReferenceNode(String title, String repositoryUID, String sutName, String section) 
	{
		super(title);
		this.repositoryUID = repositoryUID;
		this.sutName = sutName;
		this.section = section;
	}

	/**
	 * <p>Getter for the field <code>repositoryUID</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getRepositoryUID() 
	{
		return repositoryUID;
	}

	/**
	 * <p>Getter for the field <code>sutName</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getSutName() 
	{
		return sutName;
	}

	/**
	 * <p>Getter for the field <code>section</code>.</p>
	 *
	 * @return a {@link java.lang.String} object.
	 */
	public String getSection() 
	{
		return section;
	}

	/** {@inheritDoc} */
	@Override
	public void addChildren(DocumentNode child) 
	{
		throw new RuntimeException("Reference node should not have children");
	}

	/** {@inheritDoc} */
	@Override
	public Vector<Object> marshallize() 
	{
		Vector<Object> vector = super.marshallize();
		// replace the URL set by DocumentNode. This is to maintain backwar compatibility. Shifting the indexes would break everything.
		String url = null;
		if (vector.size() == 5) {
			url = (String) vector.set(NODE_REPOSITORY_UID_INDEX, repositoryUID);
		} else {
			vector.add(NODE_REPOSITORY_UID_INDEX, repositoryUID);
		}
		vector.add(NODE_SUT_NAME_INDEX, sutName);
		vector.add(NODE_SECTION_INDEX, StringUtils.defaultString(section));
		if (isNotBlank(url)) {
			vector.add(NODE_URL_INDEX, url);
		}
		return vector;
	}

	static ReferenceNode toReferenceNode(List<Object> referenceNodeParams)
	{
		ReferenceNode node = new ReferenceNode((String) referenceNodeParams.get(NODE_TITLE_INDEX),
				(String) referenceNodeParams.get(NODE_REPOSITORY_UID_INDEX),
				(String) referenceNodeParams.get(NODE_SUT_NAME_INDEX),
				(String) referenceNodeParams.get(NODE_SECTION_INDEX));

		node.setIsExecutable((Boolean) referenceNodeParams.get(NODE_EXECUTABLE_INDEX));
		node.setCanBeImplemented((Boolean) referenceNodeParams.get(NODE_CAN_BE_IMPLEMENTED_INDEX));
		if (referenceNodeParams.size() > NODE_URL_INDEX) {
			node.setURL((String) referenceNodeParams.get(NODE_URL_INDEX));
		}
		return node;
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object o) 
	{
        if(o == null || !(o instanceof ReferenceNode))
        {
            return false;
        }

        ReferenceNode nodeCompared = (ReferenceNode)o;
        if(super.equals(o))
        {
        	return getRepositoryUID().equals(nodeCompared.getRepositoryUID()) && getSutName().equals(nodeCompared.getSutName());
        }

        return false;
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() 
	{
		return super.hashCode() + getRepositoryUID().hashCode() + getSutName().hashCode();
	}
}
