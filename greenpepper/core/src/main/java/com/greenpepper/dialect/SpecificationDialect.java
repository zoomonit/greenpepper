package com.greenpepper.dialect;

import java.io.InputStream;

/**
 * Define a way to convert an input to the HTML output that will be fed to Greenpepper.
 */
public interface SpecificationDialect {

    /**
     * Converts the content of an input to the HTML output that will be fed to GreenPepper Core
     * @param input The input content to convert.
     * @return The HTML output.
     */
    String convert(InputStream input) throws SpecificationDialectException;
}
