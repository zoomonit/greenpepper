package com.greenpepper.dialect;

import java.io.IOException;

public class SpecificationDialectException extends IOException {

    public SpecificationDialectException(String message, Exception e) {
        super(message, e);
    }

    public SpecificationDialectException(String message) {
        super(message);
    }
}
