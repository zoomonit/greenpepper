package com.greenpepper.maven.plugin.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.greenpepper.Statistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;

public class TestResultsIndex extends AbstractIndex<TestResultsIndex.TestResults> implements StatisticsListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestResultsIndex.class);

    private TestResultsIndex(File storage) {
        super(storage);
    }

    @Override
    public synchronized void notify(String name, Statistics stats, long duration) {
        getNameToInfo().put(name, new TestResults(stats, duration));
        dump();
    }

    public static TestResultsIndex newInstance(File storage) {
        TestResultsIndex testResultsIndex = new TestResultsIndex(storage);
        try {
            testResultsIndex.load();
        } catch (IOException e) {
            LOGGER.warn("Unable to load the Results index file at '{}'. Cause: {}", storage, e.getMessage());
            LOGGER.debug("Here is the full error", e);
        }
        return testResultsIndex;
    }

    @Override
    public synchronized void dump() {
        try {
            super.dump();
        } catch (IOException e) {
            LOGGER.warn("Unable to write the Results index file at '{}'. Cause: {}", this.indexFile, e.getMessage());
            LOGGER.debug("Here is the full error", e);
        }
    }

    @Override
    protected TypeReference<LinkedHashMap<String, TestResults>> getValueTypeRef() {
        return new TypeReference<LinkedHashMap<String, TestResults>>() {};
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    @SuppressWarnings("WeakerAccess")
    public static class TestResults {

        Statistics statistics = new Statistics();
        long duration;

        @SuppressWarnings("unused")
        public TestResults() { }

        TestResults(Statistics statistics, long duration) {
            this.statistics = statistics;
            this.duration = duration;
        }

        public Statistics getStatistics() {
            return statistics;
        }
    }
}
