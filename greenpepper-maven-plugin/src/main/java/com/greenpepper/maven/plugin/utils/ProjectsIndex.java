package com.greenpepper.maven.plugin.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;

public class ProjectsIndex extends AbstractIndex<ProjectsIndex.ProjectInfo> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectsIndex.class);

    public ProjectsIndex(File indexFile) {
        super(indexFile);
    }

    @Override
    protected TypeReference<LinkedHashMap<String, ProjectInfo>> getValueTypeRef() {
        return new TypeReference<LinkedHashMap<String, ProjectInfo>>() {};
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }

    public static class ProjectInfo {
        public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

        public String repoName;
        public String projectName;
        public String systemUnderTest;
        public String repoId;
        public String startDate;

    }
}
