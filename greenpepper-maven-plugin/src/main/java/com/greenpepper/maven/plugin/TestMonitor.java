/*
 * Copyright (c) 2007 Pyxis Technologies inc.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA,
 * or see the FSF site: http://www.fsf.org.
 */

package com.greenpepper.maven.plugin;

import com.greenpepper.Statistics;
import com.greenpepper.maven.plugin.utils.StatisticsListener;
import com.greenpepper.runner.SpecificationRunnerMonitor;
import org.apache.maven.plugin.logging.Log;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;

import static java.lang.String.format;
import static org.apache.commons.lang3.time.DurationFormatUtils.formatDurationHMS;
import static org.apache.commons.lang3.time.DurationFormatUtils.formatDurationISO;
import static org.apache.commons.lang3.time.DurationFormatUtils.formatDurationWords;

/**
 * <p>TestMonitor class.</p>
 *
 * @author oaouattara
 * @version $Id: $Id
 */
public class TestMonitor implements SpecificationRunnerMonitor
{
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("HH:mm:ss.SSS");

    protected String name;
    private HashSet<StatisticsListener> listeners = new HashSet<StatisticsListener>();
    private final Log log;

    private Long startTime;
    long duration;

    /**
     * <p>Constructor for TestMonitor.</p>
     *
     * @param log a {@link Log} object.
     * @param statisticsListener a listener that will be notified at the end of the test
     */
    public TestMonitor(Log log, StatisticsListener statisticsListener) {
        this.log = log;
        addCompletionListener(statisticsListener);
    }


    /** {@inheritDoc} */
    public void testRunning(String name) {
        this.name = name;
        this.startTime = System.currentTimeMillis();
        String time = SIMPLE_DATE_FORMAT.format(new Date());
        log.info( format("[%s] [%s] Running %s", Thread.currentThread().getName(), time, name) );
    }

    /** {@inheritDoc} */
    public void exceptionOccured(Throwable t)
    {
        log.error( "Error running specification", t );
    }

    private void addCompletionListener(StatisticsListener listener) {
        listeners.add(listener);
    }

    /** {@inheritDoc} */
    public void testDone(int rightCount, int wrongCount, int exceptionCount, int ignoreCount) {
        long endTime = System.currentTimeMillis();
        duration = endTime - startTime;
        Statistics stats = new Statistics( rightCount, wrongCount, exceptionCount, ignoreCount );
        String time = SIMPLE_DATE_FORMAT.format(new Date(endTime));
        log.info( format("[%s] [%s] %s, took %s sec %s [%s]", Thread.currentThread().getName(), time, stats,
                formatElapsedTime(duration), (stats.hasFailed() ? " <<< FAILURE! " : ""), this.name));
        for (StatisticsListener listener : listeners) {
            listener.notify(this.name, stats, duration);
        }
    }

    private String formatElapsedTime( Long runTime ) {
        NumberFormat numberFormat = NumberFormat.getInstance( Locale.ENGLISH );
        return numberFormat.format( runTime.doubleValue() / 1000 );
    }

}
