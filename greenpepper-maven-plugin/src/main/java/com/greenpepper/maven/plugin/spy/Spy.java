package com.greenpepper.maven.plugin.spy;

import com.greenpepper.spy.SpyDescription;
import com.greenpepper.util.NameUtils;

public abstract class Spy<T extends Spy> implements SpyDescription, Comparable<T> {

    private String rawName;

    public Spy(String rawName) {
        this.rawName = rawName;
    }

    @Override
    public String getName() {
        return NameUtils.toJavaIdentifierForm(this.rawName);
    }

    @Override
    public String getRawName() {
        return this.rawName;
    }

    public int hashCode() {
        return this.getName().hashCode();
    }

    @Override
    public int compareTo(T other) {
        return this.getName().compareToIgnoreCase(other.getName());
    }
}
