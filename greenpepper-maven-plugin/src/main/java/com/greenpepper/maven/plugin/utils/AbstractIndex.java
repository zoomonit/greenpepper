package com.greenpepper.maven.plugin.utils;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.TreeMap;

import static com.fasterxml.jackson.databind.MapperFeature.AUTO_DETECT_FIELDS;
import static com.fasterxml.jackson.databind.MapperFeature.CAN_OVERRIDE_ACCESS_MODIFIERS;
import static org.apache.commons.io.FileUtils.forceMkdir;

public abstract class AbstractIndex<T> {

    private static final ObjectMapper MAPPER = new ObjectMapper();
    static {
        VisibilityChecker<?> defaultVisibilityChecker = MAPPER.getDeserializationConfig().getDefaultVisibilityChecker();
        MAPPER.configure(CAN_OVERRIDE_ACCESS_MODIFIERS, true)
                .configure(AUTO_DETECT_FIELDS, true)
                .configure(SerializationFeature.INDENT_OUTPUT, true)
                .setVisibility(defaultVisibilityChecker.withFieldVisibility(JsonAutoDetect.Visibility.ANY));
    }

    protected final File indexFile;

    private LinkedHashMap<String, T> nameToInfo = new LinkedHashMap<String, T>();

    AbstractIndex(File indexFile) {
        this.indexFile = indexFile;
    }

    public void load() throws IOException {
        if (indexFile.isFile() && indexFile.canRead()) {
            nameToInfo = MAPPER.readValue(indexFile, getValueTypeRef());
        } else {
            getLogger().trace("Storage file is not readable. Starting new index at '{}'", indexFile);
        }
    }

    protected abstract TypeReference<LinkedHashMap<String, T>> getValueTypeRef();

    public synchronized void dump() throws IOException {
        forceMkdir(indexFile.getParentFile());
        MAPPER.writeValue(indexFile, nameToInfo);
    }

    public LinkedHashMap<String, T> getNameToInfo() {
        return nameToInfo;
    }

    protected abstract Logger getLogger();
}
