package com.greenpepper.maven.plugin.utils;

import com.greenpepper.Statistics;

public final class NullStatisticsListener implements StatisticsListener {
    @Override
    public void notify(String name, Statistics stats, long duration) {
        // do nothing
    }
}
