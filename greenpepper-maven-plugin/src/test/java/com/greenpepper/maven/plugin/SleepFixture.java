package com.greenpepper.maven.plugin;

import com.greenpepper.annotation.Fixture;
import com.greenpepper.annotation.FixtureMethod;

@Fixture
public class SleepFixture {

    @FixtureMethod
    public boolean sleepFor(Long millis) throws InterruptedException {
        Thread.sleep(millis);
        return true;
    }

}
