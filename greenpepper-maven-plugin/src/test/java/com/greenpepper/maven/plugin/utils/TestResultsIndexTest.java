package com.greenpepper.maven.plugin.utils;

import com.greenpepper.Statistics;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static org.apache.commons.io.FileUtils.forceDelete;
import static org.apache.commons.io.FileUtils.readFileToString;
import static org.apache.commons.io.FileUtils.toFile;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

public class TestResultsIndexTest {

    @After
    public void cleanup() throws IOException {
        File file = toFile(getClass().getResource("written-repo-results.json"));
        if (file != null && file.exists()) {
            forceDelete(file);
        }
    }

    @Test
    public void newInstanceWhenTheRequiredFileDoesntExists() throws IOException {
        TestResultsIndex testResultsIndex = TestResultsIndex.newInstance(new File("nosuchfile.json"));
        assertTrue(testResultsIndex.getNameToInfo().isEmpty());
    }

    @Test
    public void newInstanceWhenTheRequiredFileIsNotCompatitibleShouldNotFail() throws IOException {
        TestResultsIndex testResultsIndex = TestResultsIndex.newInstance(toFile(getClass().getResource("TestResultsIndexTest.class")));
        assertTrue(testResultsIndex.getNameToInfo().isEmpty());
    }

    @Test
    public void newInstanceShouldLoadTheExistingResultFile() throws IOException {
        TestResultsIndex testResultsIndex = TestResultsIndex.newInstance(toFile(getClass().getResource("manual-repo-result.json")));
        assertEquals(2, testResultsIndex.getNameToInfo().size());
        TestResultsIndex.TestResults page1 = testResultsIndex.getNameToInfo().get("page1");
        assertNotNull(page1);
        assertEquals(1, page1.statistics.exceptionCount());
        assertEquals(0, page1.statistics.ignoredCount());
        assertEquals(3, page1.statistics.rightCount());
        assertEquals(5, page1.statistics.wrongCount());
        assertNotNull(testResultsIndex.getNameToInfo().get("page2"));
    }

    @Test
    public void dumpShouldNotFailIfImpossibleToWrite() throws IOException {
        File directory = toFile(getClass().getResource("manual-repo-result.json")).getParentFile();
        TestResultsIndex testResultsIndex = TestResultsIndex.newInstance(directory);
        testResultsIndex.notify("page3", new Statistics(4,5,6,2), 12354L);
        testResultsIndex.dump();
    }


    @Test
    public void dumpShouldWriteToFile() throws IOException {
        File directory = toFile(getClass().getResource("."));
        File storage = FileUtils.getFile(directory, "written-repo-results.json");
        TestResultsIndex testResultsIndex = TestResultsIndex.newInstance(storage);
        testResultsIndex.notify("page3", new Statistics(4,5,6,2), 12354L);
        testResultsIndex.dump();

        assertTrue(storage.exists());
        String fileToString = readFileToString(storage);
        assertThat(fileToString, hasJsonPath("page3.statistics.rightCount", equalTo(4)));
        assertThat(fileToString, hasJsonPath("page3.statistics.wrongCount", equalTo(5)));
        assertThat(fileToString, hasJsonPath("page3.statistics.exceptionCount", equalTo(6)));
        assertThat(fileToString, hasJsonPath("page3.statistics.ignoredCount", equalTo(2)));
        assertThat(fileToString, hasJsonPath("page3.duration", equalTo(12354)));
    }
}