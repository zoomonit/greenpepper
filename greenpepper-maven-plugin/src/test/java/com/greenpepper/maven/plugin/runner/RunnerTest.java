package com.greenpepper.maven.plugin.runner;

import org.junit.Test;

import static org.junit.Assert.*;

public class RunnerTest {

    @Test
    public void setCmdLineTemplateNoVarString() throws Exception {
        Runner runner = new Runner();
        String cmdLineTemplate = "test normal string with $ inside";
        runner.setCmdLineTemplate(cmdLineTemplate);
        assertEquals(cmdLineTemplate, runner.getCmdLineTemplate());
    }

    @Test
    public void setCmdLineTemplateWithProcessedVar() throws Exception {
        Runner runner = new Runner();
        String cmdLineTemplate = "test with ${processed} var string inside";
        runner.setCmdLineTemplate(cmdLineTemplate);
        assertEquals(cmdLineTemplate, runner.getCmdLineTemplate());
    }

    @Test
    public void setCmdLineTemplateWithNonProcessedVar() throws Exception {
        Runner runner = new Runner();
        String cmdLineTemplate = "test with {nonprocessed} var string inside";
        runner.setCmdLineTemplate(cmdLineTemplate);
        assertEquals("test with ${nonprocessed} var string inside", runner.getCmdLineTemplate());
    }

}