/*
 * Copyright (c) 2007 Pyxis Technologies inc.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA,
 * or see the FSF site: http://www.fsf.org.
 */

package com.greenpepper.maven.plugin;

import static com.greenpepper.maven.plugin.SpecificationDownloaderMojoTest.createLocalRepositoryAndAddItToMojo;
import static com.greenpepper.maven.plugin.runner.Runner.DEFAULT_RUNNER_NAME;
import static com.greenpepper.util.CollectionUtil.toVector;
import static org.apache.commons.io.FileUtils.*;
import static org.easymock.EasyMock.*;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.containsString;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import com.greenpepper.maven.plugin.runner.Runner;
import com.greenpepper.maven.plugin.utils.RepositoryIndex;
import com.greenpepper.runner.SpecificationRunnerMonitor;
import com.greenpepper.runner.dialect.MarkdownDialect;
import com.greenpepper.server.domain.Specification;
import com.greenpepper.server.domain.SystemUnderTest;
import org.apache.commons.io.FileUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.AbstractMojoExecutionException;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.testing.AbstractMojoTestCase;
import org.apache.maven.plugin.testing.stubs.MavenProjectStub;
import org.apache.xmlrpc.WebServer;
import org.hamcrest.CoreMatchers;
import org.jmock.Mock;
import org.jmock.core.Constraint;
import org.jmock.core.constraint.IsEqual;
import org.jmock.core.matcher.InvokeOnceMatcher;
import org.jmock.core.stub.ReturnStub;

import com.greenpepper.runner.repository.AtlassianRepository;
import com.greenpepper.util.IOUtil;
import com.greenpepper.util.URIUtil;

import io.github.lukehutch.fastclasspathscanner.FastClasspathScanner;

public class SpecificationRunnerMojoTest extends AbstractMojoTestCase
{
    private SpecificationRunnerMojo mojo;
    private WebServer ws;
    private Mock handler;

    protected void tearDown() throws Exception
    {
        super.tearDown();
        stopWebServer();
    }

    public void setUp() throws Exception
    {
        super.setUp();
        URL pomPath = SpecificationRunnerMojoTest.class.getResource( "pom-runner.xml");
        mojo = (SpecificationRunnerMojo) lookupMojo( "run", URIUtil.decoded(pomPath.getPath()) );
        mojo.project = new MavenProjectStub();
        mojo.project.setBasedir(new File("."));
        mojo.classpathElements = new ArrayList<String>( );
        String core = dependency( "greenpepper-core.jar" ).getAbsolutePath();
        mojo.classpathElements.add( core );
        mojo.classpathElements.add( dependency( "guice-1.0.jar" ).getAbsolutePath());
        
        mojo.pluginDependencies = new ArrayList<Artifact>();
        File extension = dependency( "greenpepper-extensions-java.jar" );
        mojo.pluginDependencies.add( new DependencyArtifact( "greenpepper-extensions-java", extension  ));

        mojo.pluginDependencies.add( new DependencyArtifact( "slf4j-api", dependency("slf4j-api-1.6.1.jar")));
        mojo.pluginDependencies.add( new DependencyArtifact( "jcl-over-slf4j", dependency("jcl-over-slf4j-1.6.1.jar")));

		assertEquals("en", mojo.locale);
		assertEquals(MySelector.class.getName(), mojo.selector);
		assertTrue(mojo.debug);
        mojo.refresh = true;
    }

    private Repository createLocalRepository(String name) throws URISyntaxException {
        return createLocalRepositoryAndAddItToMojo(name, localPath(), mojo);
    }

    private String localPath() throws URISyntaxException {
        return localDir().getAbsolutePath();
    }

    private File localDir() throws URISyntaxException {
        return spec("spec.html").getParentFile();
    }

    private File dependency(String name) throws URISyntaxException
    {
        return new File( classesOutputDir(), name );
    }

    private File classesOutputDir()
        throws URISyntaxException
    {
        return localDir().getParentFile().getParentFile().getParentFile().getParentFile();
    }

    public void testCanRunASingleFileSpecification() throws Exception
    {
        createLocalRepository( "repo" ).addTest( "right.html" );
        mojo.execute();
        
        assertReport( "right.html" );
    }

    public void testShouldSupportSpecifyingCustomSystemUnderDevelopmentSuchAsGuice() throws Exception
    {
        createLocalRepository( "repo" ).addTest( "guice.html" );
        mojo.systemUnderDevelopment = "com.greenpepper.extensions.guice.GuiceSystemUnderDevelopment";
        mojo.execute();

        assertReport( "guice.html" );
    }

    public void testCanRunASuiteOfSpecifications() throws Exception
    {
        createLocalRepository( "repo" ).addSuite( "/suite" );
        try
        {
            mojo.execute();
        }
        catch (MojoFailureException ignored) { }
        assertEquals(2, mojo.testResultsIndexes.get("repo").getNameToInfo().size());
        assertNonFlattenReport( "suite/right.html" );
        assertNonFlattenReport( "suite/wrong.html" );
    }

    public void testSupportsMultipleRepositories() throws Exception
    {
        createLocalRepository( "repo" ).addTest( "right.html" );
        createLocalRepository( "repo" ).addTest( "wrong.html" );

        try
        {
            mojo.execute();
        }
        catch (MojoFailureException ignored)
        {
        }
        assertReport( "right.html" );
        assertReport( "wrong.html" );
    }

    public void testRunAllTestFromRepositoryWithProjectNameAndSUT() throws Exception
    {
        Repository repo = createLocalRepository("repo");
        repo.setRoot(localPath()+"/suite");
        repo.setProjectName("Project");
        repo.setSystemUnderTest("SUT");
        try
        {
            mojo.refresh = true;
            mojo.execute();
        }
        catch (MojoFailureException ignored)
        {
        }
        assertReport( "right.html" );
        assertReport( "wrong.html" );
    }

    @SuppressWarnings("unchecked")
	public void testShouldSupportCustomRepositoriesSuchAsConfluence() throws Exception
    {
        startWebServer();
    	Vector<?> expected = toVector( "SPACE", "PAGE", Boolean.TRUE, Boolean.TRUE  );
        String right = IOUtil.readContent( spec( "right.html" ) );
        handler.expects( new InvokeOnceMatcher( ) ).method( "getRenderedSpecification" ).with( eq( "" ), eq( "" ), eq( expected ) ).will( new ReturnStub( right ) );

        createAtlassianRepository().addTest("PAGE");
        mojo.execute();

        handler.verify();
        assertReport( "PAGE.html" );
    }

    public void testShouldFailIfSingleTestLaunchedAndDefaultRepositoryNotSet() throws MojoFailureException, URISyntaxException {
        createLocalRepository( "repo1" );
        createLocalRepository( "repo2" );
        mojo.testSpecification = "right.html";
        try {
            mojo.execute();
            fail("No default repository set, the test should not run");
        } catch (MojoExecutionException e) {
            assertEquals("A default repository should be set when using '-Dgp.test='. Use '-Dgp.repo=' or specify it in the pom.xml", e.getMessage());
        } 
    }
    
    public void testShouldConsiderASingleRepositoryDefinedAsDefault() throws URISyntaxException, MojoExecutionException, MojoFailureException {
        createLocalRepository( "repoSingle" );
        mojo.testSpecification = "right.html";
        mojo.execute();
        assertReport( "right.html","repoSingle" );
    }
    
    public void testShouldSupportDefaultRepositoryDefinition() throws Exception{
        createLocalRepository( "repo1" );
        createLocalRepository( "repo2" ).setDefault(true);
        mojo.testSpecification = "right.html";
        mojo.execute();
        assertReport( "right.html", "repo2" );
    }
    public void testShouldSupportSelectedRepositoryDefinition() throws Exception{
        createLocalRepository( "repo1" );
        createLocalRepository( "repo2" );
        mojo.testSpecification = "right.html";
        mojo.selectedRepository = "repo2";
        mojo.execute();
        assertReport( "right.html", "repo2" );
    }
    
    public void testShouldOnlyRunTheSpecifiedGPTest() throws Exception{
        createLocalRepository( "repoSingle" ).addTest("wrong.html");
        mojo.testSpecification = "right.html";
        mojo.execute();
        assertEquals(1, mojo.runnerStatistics.totalCount());
        assertReport( "right.html","repoSingle" );
    }
    
    public void testShouldSupportOutputFileOnSingleTestRun() throws Exception {
        createLocalRepository( "repoSingle" ).addTest("wrong.html");
        mojo.testSpecification = "right.html";
        mojo.testSpecificationOutput = "rightCustomOutput.html";
        mojo.execute();
        assertEquals(1, mojo.runnerStatistics.totalCount());
        assertReport( "rightCustomOutput.html","repoSingle" );
    }
    
    public void testShouldUseSelectedRepositoryPrior() throws Exception {
        createLocalRepository( "repo1" ).setDefault(true);
        createLocalRepository( "repo2" );
        mojo.testSpecification = "right.html";
        mojo.selectedRepository = "repo2";
        mojo.execute();
        assertEquals(1, mojo.runnerStatistics.totalCount());
        assertReport( "right.html","repo2" );
    }

    public void testShouldFailIfSelectedRepositoryNotInTheList() throws Exception {
        createLocalRepository( "repo1" ).setDefault(true);
        createLocalRepository( "repo2" );
        mojo.testSpecification = "right.html";
        mojo.selectedRepository = "repo3";
        try {
            mojo.execute();
            fail("Should have thrown an exception");
        } catch (MojoExecutionException e) {
            assertEquals("Repository 'repo3' not found in the list of repository.", e.getMessage());
        }
    }
    
    private Repository createAtlassianRepository() {
        Repository repository = new Repository();
        repository.setName("repo");
        repository.setType( AtlassianRepository.class.getName() );
        repository.setRoot("http://localhost:9005/rpc/xmlrpc?includeStyle=true&handler=greenpepper1#SPACE");
        mojo.addRepository(repository);
        return repository;
    }
    
    private Constraint eq(Object o)
    {
    	return new IsEqual(o);
    }

    public void testShouldMakeBuildFailIfThereWereTestFailures() throws Exception
    {
        createLocalRepository("repo").addTest( "wrong.html" );
        try
        {
            mojo.execute();
            fail();
        }
        catch (MojoFailureException expected)
        {
            assertTrue( true );
        }
        assertReport( "wrong.html" );
    }

    public void testShouldMakeBuildFailIfSomeTestsCouldNotBeRun() throws Exception
    {
        createLocalRepository("repo").addTest( "no_such_file.html" );
        try
        {
            mojo.execute();
            fail();
        }
        catch (MojoExecutionException expected)
        {
            assertTrue( true );
        }
    }

    public void testShouldRunInaForkedProcess() throws Exception {
        createLocalRepository( "repo" ).addTest( "right.html" );
        setClassPathForFork();
        mojo.fork = true;
        mojo.execute();

        assertReport( "right.html" );
    }

    public void testShouldGenerateOuputLogEvenWhenLoggingOnShell() throws Exception {

        File out = reportNotFlattenFileFor( "right.html-output.log", "repo");
        if (out.exists()) {
            forceDelete(out);
        }

        createLocalRepository( "repo" ).addTest( "right.html" );
        setClassPathForFork();
        mojo.fork = true;
        mojo.execute();

        assertReport( "right.html-output.log" );
    }

    public void testShouldRunIn2ForkedProcesses() throws Exception {
        Repository localRepository = createLocalRepository("repo");
        for (int i = 1; i <= 4; i++) {
            localRepository.addTest( String.format("parallel/main%d.html",i) );
        }
        setClassPathForFork();
        mojo.fork = true;
        mojo.forkCount = 2;
        mojo.execute();

        for (int i = 1; i <= 4; i++) {
            assertNonFlattenReport( String.format("parallel/main%d.html",i) );
        }
    }


    public void testShouldForkIfTheRepositoryHasASpecificRunnerl() throws Exception {

        Repository repo = createLocalRepository("repo");
        repo.addTest( "right.html" );
        repo.setRunnerName("testRunner");

        Runner runnerMock = createMock(Runner.class);
        expect(runnerMock.getName()).andReturn("testRunner").anyTimes();
        expect(runnerMock.getForkCount()).andReturn(1).anyTimes();
        runnerMock.setRedirectOutputToFile(false);
        expectLastCall();
        runnerMock.setRepositoryIndex(anyObject(RepositoryIndex.class));
        expectLastCall();
        expect(runnerMock.isIncludeProjectClasspath()).andReturn(false);
        runnerMock.execute(anyObject(Specification.class), anyObject(SystemUnderTest.class), anyString(), anyObject(SpecificationRunnerMonitor.class));
        expectLastCall();

        mojo.runners = new ArrayList<Runner>();
        mojo.runners.add(runnerMock);
        replay(runnerMock);

        mojo.fork = false;
        mojo.execute();

        // Check that the runner were used
        verify(runnerMock);
    }

    private void setClassPathForFork() {
        List<File> uniqueClasspathElements = new FastClasspathScanner().getUniqueClasspathElements();
        List<String> classpassElements = new ArrayList<String>(uniqueClasspathElements.size());
        for (File uniqueClasspathElement : uniqueClasspathElements) {
            classpassElements.add(uniqueClasspathElement.getAbsolutePath());
        }
        mojo.classpathElements.addAll(classpassElements);
    }

    public void testShouldOnlyRunNewTestsInResumeMode() throws Exception {
        Repository localRepository = createLocalRepository("repo");
        localRepository.setRoot(localPath() + "/resume");
        localRepository.setProjectName("Project");
        localRepository.setSystemUnderTest("SUT");
        try {
            mojo.execute();
            assertEquals(1, mojo.runnerStatistics.totalCount());

            setUp();
            localRepository = createLocalRepository("repo");
            localRepository.setRoot(localPath() + "/resume");
            localRepository.setProjectName("Project");
            localRepository.setSystemUnderTest("SUT");
            copyFile(getFile(localRepository.getRoot(), "right.html"), getFile(localRepository.getRoot(), "testShouldRunOnlyNewTestsInResumeMode.html"));
            mojo.resume = true;
            mojo.refresh = true;
            mojo.execute();
            assertEquals(1, mojo.runnerStatistics.totalCount());
            assertEquals(2, mojo.testResultsIndexes.get("repo").getNameToInfo().size());
        } finally {
            forceDelete(mojo.getIndexFileForRepository(localRepository));
            forceDelete(mojo.getResultsIndexFile(localRepository));
            forceDelete(getFile(localPath() + "/resume/testShouldRunOnlyNewTestsInResumeMode.html"));
        }
    }

    public void testShouldOnlyRunFailedTestsInResumeMode() throws Exception {
        Repository localRepository = createLocalRepository("repo");
        localRepository.setRoot(localPath() + "/suite");
        localRepository.setProjectName("Project");
        localRepository.setSystemUnderTest("SUT");
        try {
            try {
                mojo.execute();
            } catch (AbstractMojoExecutionException e) {
                // ignore
            }
            assertEquals(2, mojo.runnerStatistics.totalCount());

            setUp();
            localRepository = createLocalRepository("repo");
            localRepository.setRoot(localPath() + "/suite");
            localRepository.setProjectName("Project");
            localRepository.setSystemUnderTest("SUT");
            mojo.resume = true;
            try {
                mojo.execute();
                fail("Normally, the wrong.html should have been executed");
            } catch (AbstractMojoExecutionException e) {
                // good
            }
            assertEquals(1, mojo.runnerStatistics.totalCount());
            assertEquals(2, mojo.testResultsIndexes.get("repo").getNameToInfo().size());
        } finally {
            forceDelete(mojo.getIndexFileForRepository(localRepository));
            forceDelete(mojo.getResultsIndexFile(localRepository));
        }
    }

    public void testRunAllTestFromRepositoryWithProjectNameAndSUT_ForkMode() throws Exception
    {
        Repository repo = createLocalRepository("repo");
        repo.setRoot(localPath()+"/suite");
        repo.setProjectName("Project");
        repo.setSystemUnderTest("SUT");
        try
        {
            mojo.refresh = true;
            setClassPathForFork();
            mojo.fork = true;
            mojo.execute();
        }
        catch (MojoFailureException ignored)
        {
        }
        assertReport( "right.html" );
        assertReport( "wrong.html" );
    }

    public void testShouldUseDefinedJAVAOptions() throws Exception {

        createLocalRepository( "repo" ).addTest( "right.html" );
        setClassPathForFork();
        mojo.fork = true;
        String expectedOptions = "-Xms16m -Xmx512m";
        mojo.javaOptions = expectedOptions;
        mojo.execute();

        Runner runner = mojo.runnerMap.get(DEFAULT_RUNNER_NAME);
        assertThat("default runner should contain javaOptions",runner.getCmdLineTemplate(),  containsString(expectedOptions));
    }

    public void testShouldUseDefinedSUDArgsOnEmbeddedRunner() throws Exception {

        String repo = "repo";
        createLocalRepository(repo).addTest( "right.html" );
        mojo.systemUnderDevelopment = "com.greenpepper.maven.plugin.CustomSUD";
        mojo.systemUnderDevelopmentArgs = "ARG_1;ARG_2";
        mojo.execute();

        assertReport( "right.html" );
        File out = reportFileFor( "right.html-output.log", repo);
        assertThat(readFileToString(out), containsString("Argument 0 is: ARG_1"));
        assertThat(readFileToString(out), containsString("Argument 1 is: ARG_2"));
    }

    public void testShouldUseDefinedSUDArgsOnForkedRunner() throws Exception {

        String repo = "repo";
        createLocalRepository(repo).addTest( "right.html" );
        setClassPathForFork();
        mojo.fork = true;
        mojo.systemUnderDevelopment = "com.greenpepper.maven.plugin.CustomSUD";
        mojo.systemUnderDevelopmentArgs = "ARG_1;ARG_2";
        mojo.execute();

        assertReport( "right.html" );
        File out = reportFileFor( "right.html-output.log", repo);
        assertThat(readFileToString(out), containsString("Argument 0 is: ARG_1"));
        assertThat(readFileToString(out), containsString("Argument 1 is: ARG_2"));
    }

    public void testShouldUseDefinedSUDArgsOnForkedCustomRunner() throws Exception {

        String repo = "repo";
        createLocalRepository(repo).addTest( "right.html" );
        setClassPathForFork();
        mojo.fork = true;
        mojo.systemUnderDevelopment = "com.greenpepper.maven.plugin.CustomSUD";
        mojo.systemUnderDevelopmentArgs = "ARG_1;ARG_2";
        mojo.runners = new ArrayList<Runner>();
        Runner runner = new Runner();
        runner.setName("custom");
        runner.setIncludeProjectClasspath(true);
        runner.setMainClass("com.greenpepper.runner.Main");
        runner.setCmdLineTemplate("java -cp {classpaths} {mainClass} {inputPath} {outputPath} " +
                "-r {repository} -f {fixtureFactory};{fixtureFactoryArgs}");
        mojo.runners.add(runner);
        mojo.excludeDefaultRunner = true;
        mojo.execute();

        assertReport( "right.html" );
        File out = reportFileFor( "right.html-output.log", repo);
        assertThat(readFileToString(out), containsString("Argument 0 is: ARG_1"));
        assertThat(readFileToString(out), containsString("Argument 1 is: ARG_2"));
        assertThat(readFileToString(out), CoreMatchers.not(containsString("Argument 2 is: ")));
    }

    public void testCanDefineADialect() throws Exception
    {
        Repository repo = createLocalRepository("repo");
        repo.setDialect(MarkdownDialect.class.getName());
        repo.addTest( "right.md" );
        mojo.execute();

        assertReport( "right.md.html" );
    }

    public void testCanDefineADialectInForkMode() throws Exception
    {
        Repository repo = createLocalRepository("repo");
        repo.setDialect(MarkdownDialect.class.getName());
        repo.addTest( "right.md" );
        setClassPathForFork();
        mojo.fork = true;

        mojo.execute();

        assertReport( "right.md.html" );
        File out = reportFileFor( "right.md-output.log", "repo");
        assertTrue(out.exists());
        String actualLog = readFileToString(out);
        assertThat(actualLog, containsString("1 tests: 1 right, 0 wrong, 0 ignored, 0 exception(s)"));
    }

    private File reportFileFor(String input, String repoName)
    {
        return reportNotFlattenFileFor(URIUtil.flatten(input), repoName);
    }
    private File reportNotFlattenFileFor(String input, String repoName)
    {
        return new File( new File(mojo.reportsDirectory, repoName), input );
    }

    private File spec(String name) throws URISyntaxException
    {
        return new File( URIUtil.decoded(SpecificationRunnerMojoTest.class.getResource( name ).getPath()) );
    }

    private void assertReport( String reportName ) {
        assertReport(reportName, "repo");
    }
    
    private void assertReport( String reportName, String repoName ) {
        File out = reportFileFor( reportName, repoName );
        assertReportNotEmpty(out);
    }

    private void assertNonFlattenReport( String reportName) {
        File out = reportNotFlattenFileFor( reportName, "repo");
        assertReportNotEmpty(out);
    }

    private void assertReportNotEmpty(File out) {
        assertTrue(out.getPath() + " doesn't exist",  out.exists() );
        long length = out.length();
        FileUtils.deleteQuietly(out);
        assertTrue(out.getPath() + " should not be empty",  length > 0 );
    }

    private void startWebServer()
    {
        ws = new WebServer( 9005 );
        handler = new Mock( Handler.class );
        ws.addHandler( "greenpepper1", handler.proxy() );
        ws.start();
    }

    private void stopWebServer()
    {
        if(ws != null) ws.shutdown();
    }

    @SuppressWarnings("WeakerAccess")
    public interface Handler {

        @SuppressWarnings("unused")
        String getRenderedSpecification(String username, String password, Vector<Object> args);
    }
}