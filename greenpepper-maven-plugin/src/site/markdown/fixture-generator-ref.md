# Custom Fixture Generator configuration

The FixtureGeneratorDefinition is meant to allow the specification of a JAVA class implementing a generation of fixtures.
 
```xml
<fixtureGenerator>
    <impl/>
    <properties>
        <javaBeanProperty1/>
        <javaBeanProperty2/>
        ...
        <javaBeanPropertyn/>
    </properties>
</fixtureGenerator>
```

## Nodes details 

### fixtureGenerator

| Element  | Description | Type |
|---|---|---|
| **impl** | The implementation class that will generate the fixtures. See below for more details. | String |
| **properties** | The map of JAVA Bean properties that can be injected in the implementation. | Map |

### impl

The implementation should:
 * implement the `com.greenpepper.spy.FixtureGenerator` interface.
 * have a default constructor.
 * be avalaible in the classpath during the fixture generation goal.
 
The implementation will be instanciated using the default constructor. Then the properties will be set using [Java Beans] 
specification.

[Java Beans]: http://www.oracle.com/technetwork/java/javase/documentation/spec-136004.html

### properties

The `properties` node is optional and can be empty.

When not empty, it may contain **writeable** [Java Beans] properties. This means that at least at setter is to be defined 
in the `impl` class for that property. 

The injection of properties is made using [Commons BeanUtils](https://commons.apache.org/proper/commons-beanutils/). This
has the benefits to map the properties values to Java types different from `java.lang.String`. 


## Default Fixture Generator Definition

As an information, here is the default fixture generator definition

```xml
<fixtureGenerator>
    <impl>com.greenpepper.maven.plugin.spy.impl.JavaFixtureGenerator</impl>
    <properties>
        <defaultPackage>${greenpepper.fixtures.defaultpackage}</defaultPackage>
    </properties>
</fixtureGenerator>
```
