# Usage

When starting your development, usually you already have your specification ready. 
Then you will need to create your fixtures. The goal `generate-fixtures` will help 
generate the required fixtures out of a specification. 

Let's consider the following configuration in your pom.xml

```xml
<plugin>
    <groupId>com.github.strator-dev.greenpepper</groupId>
    <artifactId>greenpepper-maven-plugin</artifactId>
    <version>${project.version}</version>
    <configuration>
        <fixtureSourceDirectory>src/fixture/java</fixtureSourceDirectory>
        <fixtureOutputDirectory>target/test-classes</fixtureOutputDirectory>
        <systemUnderDevelopment>com.greenpepper.systemunderdevelopment.GreenPepperSystemUnderDevelopment</systemUnderDevelopment>

        <repositories>
            <repository>
                <type>com.greenpepper.repository.FileSystemRepository</type>
                <root>${basedir}/src/specs/greenpepper</root>
                <name>GreenPepper-specifications</name>
            </repository>
        </repositories>
    </configuration>
</plugin>
```

and the following directory tree.

```
└── src
    └── specs
        └── greenpepper
            ├── neutral.html
            ├── right.html
            └── wrong.html
```

To generate the fixtures for all the specifications:

```bash
mvn greenpepper:generate-fixtures
```

You can also generate the fixtures for the `right.html` page only with:

```bash
mvn greenpepper:generate-fixtures -Dgp.test=right.html
```

Specification filters are also available: 

```bash
# launch only wrong.html
mvn greenpepper:generate-fixtures -Dgp.specFilter=ong
```

> **Note:** About the default package
>
> You might have the following warning when generating your fixtures:
>
> `[WARNING] You have no default package defined. I can't choose any import packages. Generating the Fixture in the source root. This is generally not a good idea.`
> 
> This can be avoided by specifying the `defaultPackage` option.

## Using a Custom Fixture Generator

A custom fixture generator can be used to generate non-Java fixtures for instance.


### Example

The following FixtureGenerator may be used with the below configuration.

```java
public class NoopFixtureGenerator implements FixtureGenerator {

    Boolean boolean1;
    BigDecimal number2;

    public void setBoolean1(Boolean boolean1) {
        this.boolean1 = boolean1;
    }

    public void setNumber2(BigDecimal number2) {
        this.number2 = number2;
    }

    @Override
    public Result generateFixture(FixtureDescription fixture, MetaInformation metaInformation, File fixtureSourceDirectory) throws Exception {
        File noopFile = File.createTempFile(fixture.getName(),".lang", fixtureSourceDirectory);
        return new Result(ActionDone.NONE, noopFile);
    }
}
```


```xml
<configuration>
    ...
    <fixtureGenerator>
        <impl>NoopFixtureGenerator</impl>
        <properties>
            <boolean1>true</boolean1>
            <number2>2.0</number2>
        </properties>
    </fixtureGenerator>
</configuration>
```

