
The html index file accesses the other files (Index, results) next to it. It uses Javascript to load those files.

This works correctly when using other browsers than Chrome.

On Chrome, it is considered as a security risk. Fortunately, there is a switch to allow the file access.


## Launch a local webserver

Ensure that python is installed on your OS. Then move to the target/greenpepper-reports folder (using a terminal) and launch :

```
python -m SimpleHTTPServer
```

This will output the following:

```
Serving HTTP on 0.0.0.0 port 8000 ...
```

You can then access the reports folder from [http://localhost:8000](http://localhost:8000)

> **Tip:** For information
> 
> You can work around this kind of issues with chrome using the following.
> Be sure to not have chrome already opened. Then launch the following command 
> 
> On Linux
> ```
> google-chrome  --allow-file-access-from-files --allow-file-access
> ```

