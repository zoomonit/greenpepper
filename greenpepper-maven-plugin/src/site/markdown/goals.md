# Plugin Documentation

Goals available for this plugin:

{children}

## System Requirements

The following specifies the minimum requirements to run this Maven plugin:

| --- | --- |
| Maven | 2.0 |
| JDK	| 1.6 |
| Memory | No minimum requirement. |
| Disk Space | No minimum requirement. |

## Usage

You should specify the version in your project's plugin configuration:

```xml
<project>
  ...
  <build>
    <!-- To define the plugin version in your parent POM -->
    <pluginManagement>
      <plugins>
        <plugin>
          <groupId>com.github.strator-dev.greenpepper</groupId>
          <artifactId>greenpepper-maven-plugin</artifactId>
          <version>${version}</version>
        </plugin>
        ...
      </plugins>
    </pluginManagement>
    <!-- To use the plugin goals in your POM or parent POM -->
    <plugins>
      <plugin>
        <groupId>com.github.strator-dev.greenpepper</groupId>
        <artifactId>greenpepper-maven-plugin</artifactId>
        <version>${version}</version>
      </plugin>
      ...
    </plugins>
  </build>
  ...
</project>
```

For more information, see [Guide to Configuring Plug-ins](http://maven.apache.org/guides/mini/guide-configuring-plugins.html)