# Calculator Example

| Begin info |
|------------|

This main purpose of this example is to show you :

* How to handle business rules using the **Rule For** Interpreter
* How to use the keyword **error** to handle exceptions

The simplicity of the Calculator example allows to avoid confusion between the 
system context and the specific behavior of GreenPepper Software.

Another particularity of this example is that we do not need to write a custom 
fixture : the **Calculator** class will be use directly.

| End info |
|----------|


| import |
|--------|
| com.greenpepper.samples.application.calculator |



| Rule for | calculator |
| x | y | sum? | product? | quotient? |
|---|---|------|----------|-----------|
| 0	| 0	| 0	   | 0        | error     |
| 1	| 0	| 1	   | 0        | error     |
| 0	| 1	| 1	   | 0        | 0         |
| 1	| 1	| 2	   | 1        | 1         |
| 10| 2 | 12   | 20       | 5         | 
