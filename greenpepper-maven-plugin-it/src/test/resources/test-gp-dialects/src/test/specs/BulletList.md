Here is an example of what I did (in MD syntax)

## Issue #134 : Generate fixtures failing with bullet points

| import |
|--------|
|com.greenpepper.demo.fixtures|


### Working case


| do with | my Fixture|
|----|----|

* say hello to **john**
* **check** the answer of **john** is **hello**
* **accept** I wave my hand to **john**

| End |
|----|

### Broken case


| do with | my Fixture|
|----|----|

* say hello to **john**
* **check** the answer of **john** is **hello**

* **accept** I wave my hand to **john**

| End |
|----|

### Working case (tips for correction)



| do with | my Fixture|
|----|----|

* say hello to **john**
* **check** the answer of **john** is **hello**

I describe here what I will do

* **accept** I wave my hand to **john**

| End |
|----|