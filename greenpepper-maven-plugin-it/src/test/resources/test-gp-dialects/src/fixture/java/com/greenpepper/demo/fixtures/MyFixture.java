package com.greenpepper.demo.fixtures;

import com.greenpepper.annotation.Fixture;
import com.greenpepper.annotation.FixtureMethod;

@Fixture(usage = "myFixture")
public class MyFixture {

	@FixtureMethod(usage = "I wave my hand to")
	public boolean iWaveMyHandTo(String param1) {
		return true;
	}

	@FixtureMethod(usage = "say hello to")
	public void sayHelloTo(String param1) {}

	@FixtureMethod(usage = "the answer of is")
	public String theAnswerOfIs(String param1) {
		return "hello";
	}
}