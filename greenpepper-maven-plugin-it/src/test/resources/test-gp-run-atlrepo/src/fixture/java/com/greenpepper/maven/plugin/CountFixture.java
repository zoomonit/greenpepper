package com.greenpepper.maven.plugin;

public class CountFixture
{
    public String countTo(Integer number) {
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i <= number; i++) {
            sb.append(" ").append(i);
        }
        return sb.toString().trim();
    }
}
