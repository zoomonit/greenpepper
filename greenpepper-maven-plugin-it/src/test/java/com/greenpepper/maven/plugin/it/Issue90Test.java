package com.greenpepper.maven.plugin.it;


import org.apache.maven.it.Verifier;
import org.apache.maven.it.util.FileUtils;
import org.apache.maven.it.util.ResourceExtractor;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.matchers.JUnitMatchers;

import java.io.File;
import java.util.ArrayList;

import static com.greenpepper.maven.plugin.it.GreenPepperMavenPluginTest.testLaunchingMaven;
import static org.apache.maven.it.util.FileUtils.fileRead;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

public class Issue90Test {

    @Test
    public void shouldSetTheArgsIntoTheSUDWhenTheDefaultRunnerIsUsed() throws Exception {
        File testDir = ResourceExtractor.simpleExtractResources(getClass(), "/issue90-fixtureFactoryArgs-defaultrunner");
        Verifier verifier = testLaunchingMaven(testDir, new ArrayList<String>(), "integration-test");

        verifier.verifyTextInLog("Argument 0 is: SUD_ARGS1");
        verifier.verifyTextInLog("Argument 1 is: SUD_ARGS2");
    }

    @Test
    public void shouldSetTheArgsIntoTheSUDWhenTheCustomRunnerIsUsed() throws Exception {
        File testDir = ResourceExtractor.simpleExtractResources(getClass(), "/issue90-fixtureFactoryArgs-customrunner");
        Verifier verifier = testLaunchingMaven(testDir, new ArrayList<String>(), "integration-test");

        String specLogsPath = "target/greenpepper-reports/file/counters/count-1.html-output.log";
        verifier.assertFilePresent(specLogsPath);

        File specLogFile = new File(verifier.getBasedir(), specLogsPath);
        assertThat(fileRead(specLogFile), containsString("Argument 0 is: ARG1"));
        assertThat(fileRead(specLogFile), containsString("Argument 1 is: ARG2"));
        assertThat(fileRead(specLogFile), not(containsString("Argument 2 is: ")));
    }
}
