package com.greenpepper.maven.plugin.it;

import org.apache.commons.io.FileUtils;
import org.apache.maven.it.VerificationException;
import org.apache.maven.it.Verifier;
import org.apache.maven.it.util.ResourceExtractor;
import org.apache.xmlrpc.WebServer;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.*;

import static com.greenpepper.maven.plugin.it.GreenPepperMavenPluginTest.testLaunchingMaven;
import static com.greenpepper.maven.plugin.it.GreenPepperMavenPluginTest.testLaunchingMavenWithErrors;

public class MavenPluginWithAtlassianRepoTest {

    @SuppressWarnings("WeakerAccess")
    public interface Handler
    {
        String getRenderedSpecification(String username, String password, Vector<?> args);
        Vector<?> getSpecificationHierarchy(String username, String password, Vector<?> args);
        Vector<?> getSystemUnderTestsOfProject(String projectName);
        Vector<?> getAllSpecificationRepositories();
        Vector<?> getSpecificationHierarchy(Vector<?> repository,Vector<?> sut);
    }

    private static WebServer ws;
    private Mockery context = new JUnit4Mockery();
    private Handler handler;

    @BeforeClass
    public static void createWebServer() {
        ws = new WebServer( 19005 );
        ws.start();
    }

    @AfterClass
    public static void shutTheWebServerDown() {
        ws.shutdown();
    }

    @Before
    public void setUp()
    {
        handler = context.mock( Handler.class );
        ws.removeHandler( "greenpepper1" );
        ws.addHandler( "greenpepper1", handler );
    }

    private class HandlerExpectations extends Expectations {

        boolean withUrl;

        void expectWithoutPageDownload() {
            atLeast(1).of(handler).getSystemUnderTestsOfProject("PROJECT");
            Vector<Vector<?>> suts = systemUnderTests();
            will(returnValue(suts));
            exactly(1).of(handler).getAllSpecificationRepositories();
            Vector<Vector<?>> specsRepos = specificationRepositories();
            will(returnValue(specsRepos));
            atLeast(1).of(handler).getSpecificationHierarchy(specsRepos.get(0), suts.get(0));
            will(returnValue(specHierarchy(withUrl)));
        }
    }

    private String specification() {
        return "<html>\n" +
                "<table border=\"1\" cellspacing=\"0\">\n" +
                "    <tr>\n" +
                "        <td>do with</td>\n" +
                "        <td colspan=\"2\">com.greenpepper.maven.plugin.CountFixture</td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "        <td>check</td>\n" +
                "        <td>count to</td>\n" +
                "        <td>9</td>\n" +
                "        <td>1 2 3 4 5 6 7 8 9</td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "        <td>end</td>\n" +
                "    </tr>\n" +
                "</table>\n" +
                "</html>";
    }

    private Vector<Object> specHierarchy(boolean withUrl)
    {
        Vector<Object> hierachy = new Vector<Object>();
        hierachy.add("ROOT");
        hierachy.add(Boolean.FALSE);
        hierachy.add(Boolean.FALSE);
        Hashtable<String,Vector<Object>> pageBranch = new Hashtable<String,Vector<Object>>();
        hierachy.add(pageBranch);

        Vector<Object> page = new Vector<Object>();
        page.add("PAGE");
        page.add(Boolean.FALSE);
        page.add(Boolean.FALSE);
        page.add(new Hashtable<String,Vector<?>>());
        if (withUrl) {
            page.add("URL source of PAGE");
        }
        pageBranch.put("PAGE", page);

        Vector<Object> page2 = new Vector<Object>();
        page2.add("PAGE Executable");
        page2.add(Boolean.TRUE);
        page2.add(Boolean.FALSE);
        Hashtable<String, Vector<?>> subPageBranch = new Hashtable<String, Vector<?>>();
        page2.add(subPageBranch);
        if (withUrl) {
            page2.add("URL source of PAGE Executable");
        }

        Vector<Object> subpage = new Vector<Object>();
        subpage.add("SUBPAGE IMPLEMENTED");
        subpage.add(Boolean.TRUE);
        subpage.add(Boolean.TRUE);
        subpage.add(new Hashtable<String,Vector<?>>());
        if (withUrl) {
            subpage.add("URL source of SUBPAGE IMPLEMENTED");
        }
        subPageBranch.put("SUBPAGE IMPLEMENTED", subpage);
        pageBranch.put("PAGE Executable", page2);

        return hierachy;
    }

    @SuppressWarnings("unchecked")
    private Vector<Vector<?>> specificationRepositories() {
        Vector<Vector<?>> repos = new Vector<Vector<?>>();
        repos.add(new Vector(Arrays.asList("REPO NAME", "Confluence-SPACE KEY" )));
        repos.add(new Vector(Arrays.asList("SUTNAME 2", "Confluence-SPACE 2 KEY" )));
        return repos;
    }

    @SuppressWarnings("unchecked")
    private Vector<Vector<?>> systemUnderTests() {
        Vector<Vector<?>> suts = new Vector<Vector<?>>();
        suts.add(new Vector(Collections.singletonList("SUTNAME")));
        suts.add(new Vector(Collections.singletonList("SUTNAME 1")));
        return suts;
    }

    @Test
    public void shouldAskForTheNotImplementedVersionOfTheSpecs() throws IOException, VerificationException {
        context.checking(new HandlerExpectations() {
            {
                this.withUrl = true;
                expectWithoutPageDownload();
                atLeast(1).of(handler).getRenderedSpecification("login", "password", new Vector<Object>() {{
                    add("SPACE KEY");add("PAGE Executable");add(Boolean.TRUE);add(Boolean.FALSE);
                }});
                will(returnValue(specification()));
                atLeast(1).of(handler).getRenderedSpecification("login", "password", new Vector<Object>() {{
                    add("SPACE KEY");add("SUBPAGE IMPLEMENTED");add(Boolean.TRUE);add(Boolean.FALSE);
                }});
                will(returnValue(specification()));
            }
        });

        File testDir = ResourceExtractor.simpleExtractResources(getClass(), "/test-gp-run-atlrepo");
        testLaunchingMaven(testDir, new ArrayList<String>(), "integration-test");
    }

    @Test
    public void shouldAskForTheNotImplementedVersionOfTheSpecsWOAuth() throws IOException, VerificationException {
        context.checking(new HandlerExpectations() {
            {
                this.withUrl = true;
                expectWithoutPageDownload();
                atLeast(1).of(handler).getRenderedSpecification("", "", new Vector<Object>() {{
                    add("SPACE KEY");add("PAGE Executable");add(Boolean.TRUE);add(Boolean.FALSE);
                }});
                will(returnValue(specification()));
                atLeast(1).of(handler).getRenderedSpecification("", "", new Vector<Object>() {{
                    add("SPACE KEY");add("SUBPAGE IMPLEMENTED");add(Boolean.TRUE);add(Boolean.FALSE);
                }});
                will(returnValue(specification()));
            }
        });

        File testDir = ResourceExtractor.simpleExtractResources(getClass(), "/test-gp-run-atlrepo-noauth");
        testLaunchingMaven(testDir, new ArrayList<String>(), "integration-test");
    }

    @Test
    public void shouldNotHangs_issue95() throws IOException, VerificationException {
        context.checking(new HandlerExpectations() {
            {
                this.withUrl = true;
                expectWithoutPageDownload();
                atLeast(1).of(handler).getRenderedSpecification("login", "password", new Vector<Object>() {{
                    add("SPACE KEY");add("PAGE Executable");add(Boolean.TRUE);add(Boolean.FALSE);
                }});
                will(returnValue(FileUtils.readFileToString(
                        FileUtils.toFile(getClass().getResource("/issue95-global-exceptions-hangs/src/specs/count-1.html")))));
                atLeast(1).of(handler).getRenderedSpecification("login", "password", new Vector<Object>() {{
                    add("SPACE KEY");add("SUBPAGE IMPLEMENTED");add(Boolean.TRUE);add(Boolean.FALSE);
                }});
                will(returnValue(specification()));
            }
        });

        File testDir = ResourceExtractor.simpleExtractResources(getClass(), "/issue95-global-exceptions-hangs");
        Verifier verifier = testLaunchingMavenWithErrors(testDir, new ArrayList<String>(), "integration-test");
        verifier.verifyTextInLog("PAGE Executable (Repository: atlrepo) (1 tests: 0 right, 0 wrong, 0 ignored, 1 exception(s))");

    }

}
