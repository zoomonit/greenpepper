package com.greenpepper.maven.plugin.it;

import com.greenpepper.GreenPepperCore;
import org.apache.maven.it.VerificationException;
import org.apache.maven.it.Verifier;
import org.apache.maven.it.util.ResourceExtractor;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class GreenPepperMavenPluginTest {

    static Verifier testLaunchingMaven(File testBasedir, List<String> cliOptions, String ... goals) throws IOException, VerificationException {
        Verifier verifier = testLaunchingMavenWithErrors(testBasedir, cliOptions, goals);
        verifier.verifyErrorFreeLog();
        return verifier;
    }

    static Verifier testLaunchingMavenWithErrors(File testBasedir, List<String> cliOptions, String ... goals) throws IOException, VerificationException {
        Verifier verifier = new Verifier(testBasedir.getAbsolutePath());
        verifier.deleteArtifact("dummy", "dummy", "4.0", "jar");
        verifier.setCliOptions(cliOptions);
        verifier.executeGoals(Arrays.asList(goals));
        verifier.resetStreams();
        return verifier;
    }

    @Test
    public void testGreenpepperPlugin() throws Exception {
        File testDir = ResourceExtractor.simpleExtractResources(getClass(), "/test-gp-resources");
        testLaunchingMaven(testDir, new ArrayList<String>(), "integration-test");
    }

    @Test
    public void testLaunchingOutsideOfProjectRoot() throws VerificationException, IOException {
        File testDir = ResourceExtractor.simpleExtractResources(getClass(), "/test-gp-resources");
        testDir = testDir.getParentFile();
        ArrayList<String> cliOptions = new ArrayList<String>() {{
            add("-f");
            add("test-gp-resources/pom.xml");
        }};
        testLaunchingMaven(testDir, cliOptions, "integration-test");
    }

    @Test
    public void testGreenpepperPluginTree() throws Exception {
        File testDir = ResourceExtractor.simpleExtractResources(getClass(), "/test-gp-multimodule");
        Verifier verifier = testLaunchingMaven(testDir, new ArrayList<String>(), "greenpepper:tree");
        verifier.verifyTextInLog("Child 1");
        verifier.verifyTextInLog("Child 2");
    }

    @Test
    public void testGreenpepperPluginGenFixture() throws Exception {
        File testDir = ResourceExtractor.simpleExtractResources(getClass(), "/test-gen-fixtures");
        Verifier verifier = testLaunchingMaven(testDir, new ArrayList<String>() {{
            add("-Dgp.test=right.html");
        }}, "greenpepper:generate-fixtures");
        verifier.verifyTextInLog("Generating fixture for : right.html");
        verifier.assertFilePresent("src/fixture/java/com/greenpepper/maven/plugin/EchoFixture.java");
    }

    @Test
    public void testGreenpepperPluginGenFixtureCustomFixtureGenerator() throws Exception {
        File testDir = ResourceExtractor.simpleExtractResources(getClass(), "/test-gen-fixtures-custom");
        Verifier verifier = testLaunchingMaven(testDir, new ArrayList<String>() {{
            add("-Dgp.test=right.html");
            add("-X");
        }}, "greenpepper:generate-fixtures");
        verifier.verifyTextInLog("Found a fixture generator definition:");
        verifier.verifyTextInLog("Generating fixture for : right.html");
        verifier.assertFilePresent("src/fixture/java/com/toto/tata/EchoFixture.java");
    }


    @Test
    public void testGreenpepperPluginGenFixtureCustomFixtureGeneratorExternalToProject() throws Exception {
        File testDir = ResourceExtractor.simpleExtractResources(getClass(), "/test-gen-fixtures-custom-dependencies");
        Verifier verifier = testLaunchingMaven(testDir, new ArrayList<String>() {{
            add("-Dgp.test=right.html");
            add("-X");
        }}, "greenpepper:generate-fixtures");
        verifier.verifyTextInLog("Found a fixture generator definition:");
        verifier.verifyTextInLog("com.greenpepper.maven.plugin.FixtureGeneratorMojoTest$NoopFixtureGenerator");
    }

    @Test
    public void testGreenpepperPluginForkedRun() throws Exception {
        File testDir = ResourceExtractor.simpleExtractResources(getClass(), "/test-gp-run-forks-defaultrunner");
        Verifier verifier = testLaunchingMaven(testDir, Collections.singletonList("-X"), "integration-test");
        verifier.verifyTextInLog("Running /counters/");
        verifier.verifyTextInLog(String.format("[GP Core %s-0]", GreenPepperCore.VERSION));
        verifier.verifyTextInLog(String.format("[GP Core %s-1]", GreenPepperCore.VERSION));
        verifier.verifyTextInLog("0 tests: 0 right, 0 wrong, 0 ignored, 0 exception(s)");
    }

    @Test
    public void testGreenpepperPluginForkedRunWithNoRunner() throws Exception {
        File testDir = ResourceExtractor.simpleExtractResources(getClass(), "/test-gp-run-forks-norunner");
        Verifier verifier = testLaunchingMaven(testDir, new ArrayList<String>(), "integration-test");
        verifier.verifyTextInLog("No runner found for executing /counters/");
    }

    @Test
    public void testGreenpepperPluginForkedRunWithCustomRunner() throws Exception {
        File testDir = ResourceExtractor.simpleExtractResources(getClass(), "/test-gp-run-forks-customrunner");
        Verifier verifier = testLaunchingMaven(testDir, new ArrayList<String>(), "integration-test");
        verifier.verifyTextInLog("[custom-0]");
        verifier.verifyTextInLog("[custom-1]");
        verifier.verifyTextInLog("Running /counters/");
        verifier.verifyTextInLog("0 tests: 0 right, 0 wrong, 0 ignored, 0 exception(s)");
    }

    @Test
    public void testGreenpepperPluginGenFixture_issue124() throws Exception {
        File testDir = ResourceExtractor.simpleExtractResources(getClass(), "/issue124-gen-fixtures-md");
        Verifier verifier = testLaunchingMaven(testDir, new ArrayList<String>() {{
            add("-Dgp.test=/md/hello.md");
        }}, "greenpepper:generate-fixtures");
        verifier.verifyTextInLog("Generating fixture for : /md/hello.md");
        verifier.assertFilePresent("src/fixture/java/HelloFixture.java");
    }

    @Test
    public void testGreenpepperPluginRun_issue125() throws Exception {
        File testDir = ResourceExtractor.simpleExtractResources(getClass(), "/issue125-subdirectory-md");
        Verifier verifier = testLaunchingMaven(testDir, new ArrayList<String>() {{
            add("-X");
        }}, "clean", "integration-test");
        verifier.verifyTextInLog("Running /md/hello.md");
        verifier.assertFilePresent("target/greenpepper-reports/file/md/hello.md.html");
    }

    @Test
    public void shouldTestAllGPSupportedDialects() throws Exception {
        File testDir = ResourceExtractor.simpleExtractResources(getClass(), "/test-gp-dialects");
        Verifier verifier = testLaunchingMaven(testDir, new ArrayList<String>(), "integration-test");
        verifier.verifyTextInLog("Running /hello.md");
        verifier.verifyTextInLog("Running /calculator.doc");
        verifier.verifyTextInLog("Running /calculator.docx");
        verifier.verifyTextInLog("Running /calculator.odt");
        verifier.verifyTextInLog("Running /right.html");
    }
}
